package httpbl

import (
	"context"
	"net"
	"strconv"
	"strings"
)

// Lookup queries the http:BL DNSBL and returns a *Result. If the IP address is
// not listed in http:BL, this method returns nil, nil.
//
// Possible errors:
//
// ErrInvalidIP:
//     ip is not an IPv4 address. Only IPv4 is currently supported.
// ErrInvalidKey:
//     The AccessKey field is unset or malformed. This does not check the
//     validity of the address key, only its layout.
// ErrInvalidResponse:
//     The DNS response did not match the format expected from the http:BL API.
//     This may signify a bug in this library or a misconfiguration of the DNS
//     resolver.
//
// Errors from the DNS resolver may also be returned by this method.
func (c *Context) Lookup(ctx context.Context, ip net.IP) (*Result, error) {
	ip = ip.To4()
	if len(ip) != 4 {
		return nil, ErrInvalidIP
	}

	if err := ValidateAccessKey(c.AccessKey); err != nil {
		return nil, err
	}

	resolver := c.Resolver
	if resolver == nil {
		resolver = net.DefaultResolver
	}

	domain := []byte(c.AccessKey)
	for i := 3; i >= 0; i-- {
		domain = append(domain, '.')
		domain = strconv.AppendUint(domain, uint64(ip[i]), 10)
	}
	domain = append(domain, ".dnsbl.httpbl.org"...)

	addrs, err := resolver.LookupIPAddr(ctx, string(domain))
	if err != nil {
		if de, ok := err.(*net.DNSError); ok && de.IsNotFound {
			err = nil
		}

		return nil, err
	}

	if len(addrs) != 1 {
		return nil, ErrInvalidResponse
	}

	r := addrs[0].IP.To4()
	if len(r) != 4 || r[0] != 127 {
		return nil, ErrInvalidResponse
	}

	if rt := ResultType(r[3]); rt != SearchEngine {
		return &Result{
			Type:   rt,
			Days:   r[1],
			Threat: r[2],
		}, nil
	}

	if r[1] != 0 {
		return nil, ErrInvalidResponse
	}

	return &Result{
		Type:         SearchEngine,
		SearchEngine: KnownSearchEngine(r[2]),
	}, nil
}

// Result holds the response from http:BL. A nil *Result signifies an NXDOMAIN
// response, meaning that the IP was not recently seen by Project Honey Pot.
//
// Accessing fields of a nil *Result will panic, but calling methods on a nil
// *Result is allowed.
type Result struct {
	// Type is a bitfield holding the types of threats recently observed
	// from the queried IP address. If Type is SearchEngine, the
	// SearchEngine field is valid. Otherwise, the Days and Threat fields
	// are valid.
	Type ResultType

	// Days is the number of days since Project Honey Pot last saw activity
	// from the queried IP address. You may wish to ignore results older
	// than a certain threshold of days as "stale".
	Days uint8
	// Threat is a logarithmic rating of how dangerous an IP is based on
	// its observed activity.
	//
	// A threat rating of 25 can be interpreted as the equivalent of
	// sending 100 spam messages to a honey pot trap. 50 corresponds to
	// the equivalent of 10,000 spam messages, and 75 corresponds to
	// 1,000,000. Due to the logarithmic scale, it is unlikely that this
	// number will rise above 200.
	//
	// A threat rating of 0 means no threat score has been assigned.
	Threat uint8

	// SearchEngine is the identified search engine type for the queried IP
	// address.
	SearchEngine KnownSearchEngine
}

// CheckThreatLevel returns true if the Result is a threat type (i.e. not a
// search engine) and the threat rating is greater than or equal to min.
func (r *Result) CheckThreatLevel(min uint8) bool {
	if r == nil || r.Type == SearchEngine {
		return false
	}

	return r.Threat >= min
}

// SeenWithinDays returns true if the Result is a threat type (i.e. not a
// search engine) and the IP's last observed activity was less than or equal to
// max days ago.
func (r *Result) SeenWithinDays(max uint8) bool {
	if r == nil || r.Type == SearchEngine {
		return false
	}

	return r.Days <= max
}

// ResultType is a bitfield of threat types.
type ResultType uint8

const (
	// SearchEngine is mutually exclusive with the other types. If a search
	// engine IP is found to be participating in spamming, it will cease to
	// be listed as a search engine IP.
	SearchEngine ResultType = 0
	// Suspicious IPs have been seen engaging in behavior that is
	// consistent with a malicious bot, but malicious behavior has not yet
	// necessarily been observed. For example, some harvesters wait nearly
	// a week before sending any spam messages to addresses they have
	// collected.
	//
	// Behavior considered supicious includes accessing many honey pots in
	// a short period of time and ignoring robots.txt.
	Suspicious ResultType = 1 << 0
	// Harvester IPs are bots that found a honey pot email address which
	// later received an email.
	Harvester ResultType = 1 << 1
	// CommentSpammer IPs are bots that attempted to leave comments on the
	// fake blog comment form served by some honey pots.
	CommentSpammer ResultType = 1 << 2
)

// String returns a string representation of the bitfield. This is mostly
// useful for debugging. There is no guarantee whatsoever to the format of the
// returned string.
func (t ResultType) String() string {
	bits := [8]string{
		"suspicious",
		"harvester",
		"comment_spammer",
		"?8?",
		"?16?",
		"?32?",
		"?64?",
		"?128?",
	}

	if t == SearchEngine {
		return "search_engine"
	}

	var flags []string
	for i := 0; i < 8; i++ {
		if t&(1<<i) != 0 {
			flags = append(flags, bits[i])
		}
	}

	return strings.Join(flags, ",")
}

// SearchEngine returns true if the result type is "search engine".
func (t ResultType) SearchEngine() bool {
	return t == SearchEngine
}

// Suspicious returns true if the "suspicious" bit is set.
func (t ResultType) Suspicious() bool {
	return t&Suspicious != 0
}

// Harvester returns true if the "harvester" bit is set.
func (t ResultType) Harvester() bool {
	return t&Harvester != 0
}

// CommentSpammer returns true if the "comment spammer" bit is set.
func (t ResultType) CommentSpammer() bool {
	return t&CommentSpammer != 0
}

// KnownSearchEngine is the identified search engine type.
type KnownSearchEngine uint8

// Search engines
const (
	SEUndocumented  KnownSearchEngine = 0
	SEAltaVista     KnownSearchEngine = 1
	SEAsk           KnownSearchEngine = 2
	SEBaidu         KnownSearchEngine = 3
	SEExcite        KnownSearchEngine = 4
	SEGoogle        KnownSearchEngine = 5
	SELooksmart     KnownSearchEngine = 6
	SELycos         KnownSearchEngine = 7
	SEMSN           KnownSearchEngine = 8
	SEYahoo         KnownSearchEngine = 9
	SECuil          KnownSearchEngine = 10
	SEInfoSeek      KnownSearchEngine = 11
	SEMiscellaneous KnownSearchEngine = 12
)

// KnownSearchEngine returns a string representation of the enum value.
func (se KnownSearchEngine) String() string {
	if se > SEMiscellaneous {
		return "?" + strconv.Itoa(int(se)) + "?"
	}

	names := [...]string{
		SEUndocumented:  "Undocumented",
		SEAltaVista:     "AltaVista",
		SEAsk:           "Ask",
		SEBaidu:         "Baidu",
		SEExcite:        "Excite",
		SEGoogle:        "Google",
		SELooksmart:     "Looksmart",
		SELycos:         "Lycos",
		SEMSN:           "MSN",
		SEYahoo:         "Yahoo",
		SECuil:          "Cuil",
		SEInfoSeek:      "InfoSeek",
		SEMiscellaneous: "Miscellaneous",
	}

	return names[se]
}
