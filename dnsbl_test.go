package httpbl_test

import (
	"context"
	"net"
	"testing"

	"git.lubar.me/ben/httpbl"
)

func TestIPv6(t *testing.T) {
	t.Parallel()

	// IPv6 is not supported by this DNSBL.

	ip := net.ParseIP("2001:db8::1234:5678")
	if ip == nil {
		panic("ParseIP failed unexpectedly")
	}

	r, err := ctx.Lookup(context.Background(), ip)
	if r != nil {
		t.Errorf("expected nil result, got %+v", r)
	}
	if err != httpbl.ErrInvalidIP {
		t.Errorf("expected invalid IP error, got %+v", err)
	}
}

func TestUnseen(t *testing.T) {
	t.Parallel()

	r, err := ctx.Lookup(context.Background(), net.IPv4(127, 0, 0, 1))
	if err != nil {
		t.Errorf("expected nil error, got %+v", err)
	}
	if r != nil {
		t.Errorf("expected nil result, got %+v", r)
	}
}

var typeTests = [...]struct {
	name       string
	ip         net.IP
	search     bool
	suspicious bool
	harvester  bool
	comment    bool
}{
	// TODO: the documented search engine test IP is 127.1.1.0, which has
	// the second octet (reserved and set to 0 for search engines) set to 1
	//{"Type0", , true, false, false, false},
	{"Type1", net.IPv4(127, 1, 1, 1), false, true, false, false},
	{"Type2", net.IPv4(127, 1, 1, 2), false, false, true, false},
	{"Type3", net.IPv4(127, 1, 1, 3), false, true, true, false},
	{"Type4", net.IPv4(127, 1, 1, 4), false, false, false, true},
	{"Type5", net.IPv4(127, 1, 1, 5), false, true, false, true},
	{"Type6", net.IPv4(127, 1, 1, 6), false, false, true, true},
	{"Type7", net.IPv4(127, 1, 1, 7), false, true, true, true},
}

func TestType(t *testing.T) {
	t.Parallel()

	for _, c := range typeTests {
		c := c // shadow for scope
		t.Run(c.name, func(t *testing.T) {
			t.Parallel()

			r, err := ctx.Lookup(context.Background(), c.ip)
			if err != nil {
				t.Errorf("unexpected error: %+v", err)
			}
			if r == nil {
				t.Error("unexpectedly nil result")
				return
			}

			check := func(expected, actual bool, name string) {
				if expected != actual {
					t.Errorf("expected %q flag to be %v, but it is %v", name, expected, actual)
				}
			}

			check(c.search, r.Type.SearchEngine(), "search engine")
			check(c.suspicious, r.Type.Suspicious(), "suspicious")
			check(c.harvester, r.Type.Harvester(), "harvester")
			check(c.comment, r.Type.CommentSpammer(), "comment spammer")
		})
	}
}

var threatTests = [...]struct {
	name     string
	ip       net.IP
	least0   bool
	least10  bool
	least25  bool
	least50  bool
	least100 bool
}{
	{"NotFound", net.IPv4(127, 0, 0, 1), false, false, false, false, false},
	{"Threat10", net.IPv4(127, 1, 10, 1), true, true, false, false, false},
	{"Threat20", net.IPv4(127, 1, 20, 1), true, true, false, false, false},
	{"Threat40", net.IPv4(127, 1, 40, 1), true, true, true, false, false},
	{"Threat80", net.IPv4(127, 1, 80, 1), true, true, true, true, false},
}

func TestThreat(t *testing.T) {
	t.Parallel()

	for _, c := range threatTests {
		c := c // shadow for scope
		t.Run(c.name, func(t *testing.T) {
			t.Parallel()

			r, err := ctx.Lookup(context.Background(), c.ip)
			if err != nil {
				t.Errorf("unexpected error: %+v", err)
			}

			check := func(expected bool, threat uint8) {
				actual := r.CheckThreatLevel(threat)
				if actual == expected {
					return
				}
				if expected {
					t.Errorf("expected threat to be at least %d", threat)
				} else {
					t.Errorf("expected threat to NOT be at least %d", threat)
				}
			}

			check(c.least0, 0)
			check(c.least10, 10)
			check(c.least25, 25)
			check(c.least50, 50)
			check(c.least100, 100)
		})
	}
}

var daysTests = [...]struct {
	name      string
	ip        net.IP
	within0   bool
	within10  bool
	within25  bool
	within50  bool
	within100 bool
}{
	{"NotFound", net.IPv4(127, 0, 0, 1), false, false, false, false, false},
	{"Days10", net.IPv4(127, 10, 1, 1), false, true, true, true, true},
	{"Days20", net.IPv4(127, 20, 1, 1), false, false, true, true, true},
	{"Days40", net.IPv4(127, 40, 1, 1), false, false, false, true, true},
	{"Days80", net.IPv4(127, 80, 1, 1), false, false, false, false, true},
}

func TestDays(t *testing.T) {
	t.Parallel()

	for _, c := range daysTests {
		c := c // shadow for scope
		t.Run(c.name, func(t *testing.T) {
			t.Parallel()

			r, err := ctx.Lookup(context.Background(), c.ip)
			if err != nil {
				t.Errorf("unexpected error: %+v", err)
			}

			check := func(expected bool, days uint8) {
				actual := r.SeenWithinDays(days)
				if actual == expected {
					return
				}
				if expected {
					t.Errorf("expected to have been seen within the past %d days", days)
				} else {
					t.Errorf("expected to have been at least %d days stale", days)
				}
			}

			check(c.within0, 0)
			check(c.within10, 10)
			check(c.within25, 25)
			check(c.within50, 50)
			check(c.within100, 100)
		})
	}
}
