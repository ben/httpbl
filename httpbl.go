// Package httpbl connects to the Project Honey Pot http:BL API.
package httpbl

import (
	"errors"
	"net"
)

// Predefined error values
var (
	ErrMissingAccessKey = errors.New("httpbl: missing access key")
	ErrInvalidKey       = errors.New("httpbl: malformed access key")
	ErrInvalidIP        = errors.New("httpbl: ip is not a valid IPv4 address")
	ErrInvalidResponse  = errors.New("httpbl: dns response does not match expected format")
)

// ValidateAccessKey validates the structure of an access key.
// Access keys consist of 12 lower-case letters and can be found at
// https://www.projecthoneypot.org/httpbl_configure.php
func ValidateAccessKey(key string) error {
	if key == "" {
		return ErrMissingAccessKey
	}

	if len(key) != 12 {
		return ErrInvalidKey
	}

	for i := 0; i < len(key); i++ {
		if key[i] < 'a' || key[i] > 'z' {
			return ErrInvalidKey
		}
	}

	return nil
}

// Context is the base type for authenticated http:BL requests.
type Context struct {
	// AccessKey is the http:BL access key from Project Honey Pot.
	// Access keys should be provided by the user, as sharing an
	// access key is against the terms of service.
	//
	// An access key can be requested at this page:
	// https://www.projecthoneypot.org/httpbl_configure.php
	AccessKey string

	// Resolver is the DNS resolver to use. If it is nil,
	// net.DefaultResolver will be used instead.
	Resolver *net.Resolver
}
