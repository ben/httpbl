package httpbl_test

import (
	"testing"

	"git.lubar.me/ben/httpbl"
)

// apiKey is NOT a valid API key. Sharing or constructing API keys outside of
// Project Honey Pot's http:BL configuration page is against the terms of
// service, but special permission has been obtained to use this string for
// testing purposes.
const apiKey = "abcdefghijkl"

var ctx = &httpbl.Context{
	AccessKey: apiKey,
}

var accessKeyTests = [...]struct {
	name string
	key  string
	err  error
}{
	{"Missing", "", httpbl.ErrMissingAccessKey},
	{"WrongLength", "invalid", httpbl.ErrInvalidKey},
	{"WrongCharacters", "abc123def456", httpbl.ErrInvalidKey},
	{"CorrectlyFormatted", apiKey, nil},
}

func TestAccessKey(t *testing.T) {
	t.Parallel()

	for _, c := range accessKeyTests {
		c := c // shadow for scope
		t.Run(c.name, func(t *testing.T) {
			t.Parallel()

			err := httpbl.ValidateAccessKey(c.key)
			if err != c.err {
				t.Errorf("expected ValidateAccessKey(%q) to return error %+v, but error is %+v", c.key, c.err, err)
			}
		})
	}
}
